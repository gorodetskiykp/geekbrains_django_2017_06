from django.contrib import admin
from .models import Hobby, Study, Work, Organization

admin.site.register(Hobby)
admin.site.register(Study)
admin.site.register(Work)
admin.site.register(Organization)
