from django.shortcuts import render
from django.views.generic import TemplateView

import datetime
from collections import namedtuple

Hobbie = namedtuple('Hobbie', ['hobbie_name', 'icon'])
Study = namedtuple('Study', ['school', 'specialization', 'logo_file', 'logo_width', 'school_url', 'certificate_url'])
Work = namedtuple('Work', ['company', 'company_logo_file', 'logo_width', 'company_url', 'specialization'])

class MyProfileView(TemplateView):
    template_name='about_me_old.html'

    def get_context_data(self, **kwargs):
        context = super(MyProfileView, self).get_context_data(**kwargs)
        context['my_name'] = 'Городецкий Константин'
        context['birth_date'] = datetime.date(1982, 8, 28)
        context['my_hobbies'] = [
            Hobbie('Программирование', 'fa-bug'),
            Hobbie('Фотография', 'fa-camera-retro'),
            Hobbie('Литература', 'fa-book')
        ]
        return context

class MyStudyView(TemplateView):
    template_name = 'study_list.html'

    def get_context_data(self, **kwargs):
        context = super(MyStudyView, self).get_context_data(**kwargs)
        context['my_studies'] = [
            Study(
                'Обучающий портал Geekbrains',
                '2015, Python. Уровень 1. Основы языка',
                'geekbrains.png',
                '200px',
                'https://geekbrains.ru/',
                'https://geekbrains.ru/certificates/21563'
            ),
            Study(
                'Учебный центр Softline',
                '2015, ITIL3F Основы ITILv3 - 2011', 'sl.jpg',
                '300px',
                'http://edu.softline.ru/',
                ''
            ),
            Study(
                'Онлайн курсы stepic.org & Bioinformatics Institute',
                '2014, Программирование на Python',
                'stepic.png',
                '450px',
                'https://stepik.org/',
                'https://stepik.org/certificate/98b265eeaa701b133b5ea88a3eb6d0779dc11bd5.pdf'
            ),
            Study(
                'Тихоокеанский государственный университет, Хабаровск',
                '2008, Институт информационных технологий, инженер-системотехник',
                'togu.jpg',
                '250px',
                'http://pnu.edu.ru/ru/',
                ''
            ),
        ]
        return context

class MyWorkView(TemplateView):
    template_name = 'work_list.html'

    def get_context_data(self, **kwargs):
        context = super(MyWorkView, self).get_context_data(**kwargs)
        context['my_works'] = [
            Work(
                'ООО «Транснефть — Дальний Восток» Хабаровск',
                'tn3.png',
                '300px',
                'http://fareast.transneft.ru',
                '2014, Начальник ИТ-сектора'
            ),
            Work(
                'БПО Нерюнгри ООО «Востокнефтепровод»',
                'tn2.jpg',
                '250px',
                'http://vostok.transneft.ru',
                '2010, Инженер-электроник'
            ),
            Work(
                'НПС-14 Олекминск ООО «Востокнефтепровод»',
                'tn1.png',
                '200px',
                'http://vostok.transneft.ru',
                '2009, Инженер-электроник'
            ),
            Work(
                'ОАО «Дальсвязь» Хабаровск',
                'dsv.png',
                '300px',
                'https://ru.wikipedia.org/wiki/Дальсвязь',
                '2008, Инженер-программист'
            ),
            Work(
                'Завод «Балтика-Хабаровск»',
                'baltika.jpg',
                '300px',
                'http://corporate.baltika.ru/plant/8/',
                '2008, Инженер-электроник'
            ),
        ]
        return context