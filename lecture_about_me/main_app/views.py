from django.shortcuts import render

def main_page(request):
    my_name = 'Константин'
    context = {'my_name': my_name}
    return render(request, 'base.html', context)

def about(request):
    return render(request, 'about.html')

def works(request):
    return render(request, 'works.html')
