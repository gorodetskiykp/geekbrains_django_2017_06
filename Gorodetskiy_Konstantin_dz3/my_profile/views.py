from django.views.generic import ListView
from .models import Hobby, Study, Work
import datetime


class MyProfileView(ListView):
    model = Hobby
    template_name = 'about_me.html'

    def get_context_data(self, **kwargs):
        context = super(MyProfileView, self).get_context_data(**kwargs)
        context['my_name'] = 'Городецкий Константин'
        context['birth_date'] = datetime.date(1982, 8, 28)
        return context


class MyStudyView(ListView):
    model = Study


class MyWorkView(ListView):
    model = Work
