"""about_me URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.contrib import admin

from django.views.generic import TemplateView
from my_profile.views import MyProfileView, MyStudyView, MyWorkView, OrganizationDetailsView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='main_page'),
    url(r'^about-me/$', MyProfileView.as_view(), name='about_me'),
    url(r'^study/$', MyStudyView.as_view(), name='study'),
    url(r'^work/$', MyWorkView.as_view(), name='work'),
    url(r'^organization/(?P<pk>[0-9]+)/$', OrganizationDetailsView.as_view(), name='organization'),

]
