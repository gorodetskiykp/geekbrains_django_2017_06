from django.db import models


class Hobby(models.Model):
    name = models.CharField('Хобби', max_length=30, blank=False)
    icon = models.CharField('Иконка', max_length=30, blank=False)

    def __str__(self):
        return self.name


class Organization(models.Model):
    name = models.CharField('Организация', max_length=100, blank=False)
    logo_file = models.CharField('Логотип', max_length=30, blank=False)
    logo_width = models.CharField('Ширина логотипа', max_length=10, default='200px', blank=False)
    organization_url = models.URLField('Ссылка на сайт', max_length=100, blank=False)
    city = models.CharField('Город', max_length=100)
    address = models.CharField('Адрес', max_length=150)
    phone = models.CharField('Телефон', max_length=100)

    def __str__(self):
        return self.name


class Study(models.Model):
    school = models.ForeignKey(Organization, verbose_name='Учебное заведение', related_name='studies')
    specialization = models.CharField('Специализация', max_length=100, blank=False)
    certificate_url = models.URLField('Ссылка на сертификат', max_length=200, blank=True)

    def __str__(self):
        return self.school.name


class Work(models.Model):
    company = models.ForeignKey(Organization, verbose_name='Компания', related_name='works')
    specialization = models.CharField('Должность', max_length=100, blank=False)

    def __str__(self):
        return self.company.name
